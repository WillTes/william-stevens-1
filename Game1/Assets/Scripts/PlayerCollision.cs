using UnityEngine;

public class PlayerCollision : MonoBehaviour
{

	public PlayerMovement movement;
	public GameObject fractured;

	void OnCollisionEnter(Collision collisionInfo)
	{

		if (collisionInfo.collider.tag == "Obstacle")
		{
			movement.enabled = false;
			FindObjectOfType<GameManager>().EndGame();

			Vector3 oldPos = transform.position;

			Instantiate(fractured, oldPos, Quaternion.identity);

			GetComponent<MeshRenderer>().enabled = false;
			GetComponent<BoxCollider>().enabled = false;
			GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;

		}
	}
}